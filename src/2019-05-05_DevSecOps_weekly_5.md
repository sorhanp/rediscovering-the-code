# DevSecOps weekly, fifth issue

This time I bring a quick update about what is going on, since this week has been really low on lectures and more about self studies, and of course [Vappu, or May Day][vappu]. 

Like said this week has been very low on lectures at the campus, thus I have had more time on my own path. At the start of the week, we had a very brief lecture about software development methods such as [KANBAN][kanban], [SCRUM][scrum] and [SCRUMBAN][scrumban], all of which made me realize that I do not keep very precise track of my path become a software developer. Thus, I decided to learn KANBAN, while making my own KANBAN-board showing status of what I have learned, what I’m currently learning and what I plan on learning in the future. The result looks like this:

![Kanban of my studies](./assets/images/KANBAN.png)

As can be seen each of the title, which I will be calling cards from now on, is also color coded. I have used colors to indicate what type of material I’m dealing with, so I can quickly glance that I have read this and this number of books or finish this and that course etc. Here is a list of what each color means:

![Kanban color codes](./assets/images/KANBAN_colorcodes.png)

So. what is KANBAN? it is a scheduling system for manufacturing and it’s goal is to improve efficiency. It was first used in car factories, where it is important to have constant flow of inventory. It then expanded on other fields, such as software development. The power of Kanban is to have visualized view of the situation to give participants a view of progress and process. People like to see process bars fill, so this has similar kind of effect. Something is left in the backlog, then moved to TODO, finally worked on and (hopefully) finished. To make it even clearer for myself and really drive home to idea of the board, I decided to make a list for this blog about each of the KANBAN-cards that I have written on the board. This post can be considered as a recap of what I have done, with a view on what I plan to learn more of. Let’s get going! Oh, and for little change of pace, I will be going from right to left, starting from what I have done, then moving on what I must finish and finally what I have planned.

## Done 
 
| Card | Description |
| Programming with Java I | [Helsinki University’s MOOC][mooc] on the subject of Java programming. My on-the-job-training workplace is using Java; so I have made my mission to learn as much of it as possible before starting as a trainee. |
| [Learn Advanced C++ Programming][c++_advanced] | Continuation of C++ Tutorial for Complete Beginners, which have been on hold because of my math studies taking over programming. Will however continue with this since it has much more modern look of the language, which I am eager to learn. |
| Khan Academy Algebra 1 | Continuation of Pre-Algebra course they offered. Dwelling even deeper in the world of mathematics.  |
| [PHP for Beginners][php_course]| A very pragmatic look in to the world of PHP, which I have been doing on the side when I feel like I want to learn more about web techniques and especially databases. |
| Audio and regular books | I have been listening on [The Complete Software Developer’s Career Guide][career_guide] every time I go outside for a jog, training my mind and body at the same time. Others I read when I have downtime and want to learn something else |
| Mandatory DevSecOps-training courses | Rest of the WIP’s are mandatory courses on DevSecOps-training that I’m currently attending. I will go on detail on these in later blog posts |

All in all, my Work In Progress looks really nice. I have something to do on all days of the week, and if some subject doesn’t *flow* well at that moment I can hop on something else. I also keep constant track of my progress on each subject by documenting them as detailed as I can to my learning journal. Most of those writings form in to a blog post in later stages, when I have put enough thought on each subject.

## TODO

Since I have a huge Work In Progress-list, I have made my TODO much lighter in term of subjects. Only the second course on “Programming with Java” (this is also Helsinki University’s MOOC) is marked on there, but when that moves to WIP-pile I’m certain that I can move at least couple of things from my backlog to the TODO-list.

## Backlog

These are the things I want to learn in the future, either by working or studying. Most of them are not categorized yet, because I have no source on where to study them. It might be a book, a course, a MOOC etc., but the idea here is to move them to TODO-list **after** I have found a good source on how to study it. I will not go into detail on these, since they are “shrouded in mystery”. Still, if you have any good sources on how to learn, or maybe even start a podcast with me, hit me up with email or a message on LinkedIn.

-sorhanp


[last]: /programming/2019/04/07/DevSecOps-weekly-4.html
[vappu]: https://en.wikipedia.org/wiki/International_Workers%27_Day
[kanban]: https://www.planview.com/resources/articles/what-is-kanban/
[scrum]: https://www.scrum.org/
[scrumban]: https://www.agilealliance.org/what-is-scrumban/
[c++_beginner]: https://www.udemy.com/free-learn-c-tutorial-beginners/
[project_wiki]: https://github.com/sorhanp/particlefire-revision/wiki
[long_division]: https://github.com/sorhanp/graphical-long-division
[wxwidgets]: https://www.wxwidgets.org/
[chatrooms]: https://github.com/sorhanp/chatrooms
[service_design]: https://sorhanp.github.io/programming/2019/04/14/DevSecOps-weekly-2.html
[mooc]: https://mooc.fi/en/
[c++_advanced]: https://www.udemy.com/learn-advanced-c-programming/
[php_course]: https://www.udemy.com/php-for-complete-beginners-includes-msql-object-oriented/
[career_guide]: https://simpleprogrammer.com/products/careerguide/free/