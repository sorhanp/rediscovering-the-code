# New beginnings

As stated in my [previous][previous] blog post I have most experience in software development using C++ -programming language, meaning I still possess some kind of basic understanding of language in question. Because of that I decided that should start my journey from the basics of C++ as a refresher.

In my opinion, it is never waste of time to start from the beginning, just to make sure that there is nothing that one have forgotten. I think it will later reduce my frustration when I face some obstacle that I could have overcome easily, if I had refreshed my memory about the basics.

Luckily finding courses that provide one with absolute basics of programming are available far and wide online. For example a simple “best courses to learn C++” Internet-search provides a lot of good pointers where to begin. My first results where 
[Digitaldefynd](https://digitaldefynd.com/best-c-plus-plus-tutorial-course-certification/) and
[Java67](http://www.java67.com/2018/02/5-free-cpp-courses-to-learn-programming.html)

Both of which provide nice and simple list that provide nice overview of the courses featured. Digitaldefynd ones were mostly about paid courses at 
[Udemy](https://www.udemy.com/) online learning portal. Java67 also provided Udemy-course that 
[caught my eye](https://www.udemy.com/free-learn-c-tutorial-beginners/learn/v4/content) with the line: “can be completed with any C++ IDE and compiler“ as mentioned previously I’m running virtual machine with Arch Linux on it. so there no need to setup any new software and I'm able to jump right in to the course. For future I also found out about Microsoft edX-courses [Introduction][edxbegin]
, which I will check out later, when I want to learn about 
[Microsoft Visual Studio][visualstudio] IDE.

So I started C++ Tutorial for Complete Beginners and straight away the course lecturer informs that C++ is perfect for AI-applications, which is one of the reasons why I decided that it is my time to start learning programming again. I got my inspiration from [Elements of AI][elementsai] online course which I completed this September. I was happy to notice, that after of couple of introductory lectures I was already writing lines of code like I used to all those years ago. It felt really nice to start making my own programs along with instructions from lecturer. At one point I even went and checked out my old material from around seven years ago. That is, because during the course I had a flashback about [Makefiles][makefile] and [Hungarian Notation][notation], so that I could include these at the very beginning and add something from the past to current learning process. I have also learned something new like [limits.h or climits][climits] and [flush][flush] that were previously not taught to me.

So after all that my current plan is that I go through the C++ Tutorial for Complete Beginners daily and then before bedtime I will rehearse what I have just learned with [Sololearn][sololearn] mobile app that I found for my Android phone that doesn’t need a own compiler installed. 

As the lecturer said, when finishing his courses: Happy coding!

-sorhanp

[previous]: ./2018-10-19_My_first_post.html
[edxbegin]: https://www.edx.org/course/introduction-to-c-3
[visualstudio]: https://visualstudio.microsoft.com/
[elementsai]: http://www.elementsofai.com/fi
[makefile]: https://en.wikipedia.org/wiki/Makefile
[notation]: http://web.mst.edu/~cpp/common/hungarian.html
[climits]: http://www.cplusplus.com/reference/climits/
[flush]: http://www.cplusplus.com/reference/ostream/ostream/flush/
[sololearn]: https://www.sololearn.com/