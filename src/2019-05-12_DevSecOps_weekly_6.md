# DevSecOps weekly, sixth issue

There has been a huge focus on testing aspect of software development during this week at my training. I learned about different testing types and wrote a simple testing suite using [Robot Framework][robot_framework]. 

## Types of testing 

Robot framework is the open source framework for acceptance testing, written using Python programing language. It is a completely independent of operating system and application, thus it can be run almost everywhere. What makes it very powerful is it’s easy to use syntax, that is completely keyword driven. Here is an example.robot-file which has a test case using Robot Framework’s syntax with SeleniumLibrary: 


    *** Settings *** 
    Resource          resource.robot 

    *** Invalid login test case starts below ***  
    Invalid Login  
        Open Browser To Login Page  
        Input Username    test@test.com 
        Submit Email  
        Input Password    test  
        Submit Password  
        Invalid Username Or Password Text Should Be Printed  
        [Teardown]    Close Browser  

As can be seen it is easy to understand, so easy in fact, that my lecturer even stated that even the CEO of the software company knows what is being tested with above syntax. 

Anyway, I stated that Robot Framework has something called SeleniumLibrary, what does it do? Well the idea of Robot Framework is that the base framework provides [libraries][libraries] with basic keywords, such as [Builtin][builtin], but these keywords can be expanded with multiple external libraries, such as SeleniumLibrary which allows web testing. In fact keyword “Close Browser“ is [SeleniumLibrary's keyword][selenium].  

If you have checked out the documentations I have provided so far, you might have noticed that there is no syntax or keyword for Open Browser To Login Page,Submit Email etc. You might have also noticed that Close Browser seems to be only syntax available. Well Robot Framework also has support for something called “high-level keywords”, which allows users to create their own keywords by combining available library keywords. For example, “Open Browser To Login Page” is actually high-level keyword that includes these keywords and variables: 

    Open Browser To Login Page 
        Open Browser    ${LOGIN URL}    ${BROWSER} 
        Maximize Browser Window 
        Set Selenium Speed    ${DELAY} 
        Login Page Should Be Open 

These high-level keywords are declared inside resource.robot-file, and you might have noticed that on example.robot-file, the first line after ***Settings*** is Resource resource.robot. This is where the high-level keywords are acquired. These high-level keywords are same as functions in regular programming. They provide a way to encapsulate regular keywords inside, thus preventing unnecessary repetition of keywords. 

High-level keywords can even call other high-level keywords like is done on “Open Browser To Login Page”.  Here “Login Page Should Be Open” is called and it looks like this: 

    Login Page Should Be Open 
        Title Should Be    Home Page 

Finally, there are variables, such as ${LOGIN URL} and ${BROWSER} these work like variables in regular programming, meaning that one can store values inside them. Here is an example of values used in resource.robot-file:

    *** Variables *** 
    ${SERVER}         testpage.test 
    ${BROWSER}        Firefox 
    ${DELAY}          0 
    ${LOGIN URL}      http://${SERVER}/ 
    ${WELCOME URL}    http://${SERVER}/welcome.html 
    ${ERROR URL}      http://${SERVER}/error.html 

Again, these are highly useful and once again prevent users from repeating the same thing again and again. Also, this makes the syntax much more modular, thus the same test case can be used in another server or browser. Simply changing the corresponding variable without need for combing through the whole file. 

## Robot Framework Example 
 
Last header covers the basics of Robot Framework, so now it is time for me to show you how it is done. 

Here is a simple YouTube-video I recorded of the Robot in action:

<iframe width="500" height="282" src="https://www.youtube.com/embed/ovKSsETfSYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

What is happening here is four different tests: 

1. Test if newest blog post is up by checking if it is possible for the robot to locate link that points to location “/programming/2019/05/05/DevSecOps-weekly-5.html”. 
2. iPhone X test for blog by resizing the window to 375 x 812 size and taking a screenshot of it. 
3. Test for blog images, in which the robot checks if it possible to locate two images that point to locations ./assets/images/KANBAN.png and ./assets/images/KANBAN_colorcodes.png 
4. View test completed; not really a test, but a redirection to image that shows that all the tests are completed. 

As seen in the second test, robot can be also told to take screenshots on any location, here is what this blog looks like on iPhone X screen:

![Blog on iPhone X screen](./assets/images/iPhoneX_screen.png) 

## Example-files
 
Here are my files, first the resource-file know as resource.robot: 

    *** Settings *** 
    Documentation     A resource file with reusable keywords and variables. 
    Library           SeleniumLibrary 

    *** Variables *** 
    ${SERVER}         sorhanp.github.io 
    ${BROWSER}        Firefox 
    ${DELAY}          2 
    ${HOME URL}       http://${SERVER}/ 
    ${HOME TITLE}     (Re)discovering the code | Programming blog of Hannupekka Sormunen 
    ${LATEST POST}    /programming/2019/05/05/DevSecOps-weekly-5.html 
    ${LATEST TITLE}   DevSecOps weekly, fifth issue | (Re)discovering the code 
    ${DEFAULT WIDTH}         1100 
    ${DEFAULT HEIGHT}        975 
    ${IPHONE WIDTH}          375 
    ${IPHONE HEIGHT}         812 

    *** Keywords *** 
    Open Browser To Home Page 
        Open Browser                ${HOME URL}    ${BROWSER} 
        Set Default Browser Size 
        Set Selenium Speed          ${DELAY} 
        Home Page Should Be Open 

    Set Default Browser Size 
        Set Window Size             ${DEFAULT WIDTH}    ${DEFAULT HEIGHT} 

    Home Page Should Be Open 
        Title Should Be             ${HOME TITLE} 

    Click The Newest Post 
        Click Link                  ${LATEST POST} 
        Title Should Be             ${LATEST TITLE} 

    Resize To iPhone X 
        Set Window Size             ${IPHONE WIDTH}    ${IPHONE HEIGHT} 

    Blog Should Contain Two Images 
        Set Selenium Speed  1 
        Page Should Contain Image   ./assets/images/KANBAN.png 
        Page Should Contain Image   ./assets/images/KANBAN_colorcodes.png 
        Set Selenium Speed  ${DELAY} 

    Scroll Through Page 
        Set Selenium Speed  0 
        : FOR    ${INDEX}    IN RANGE    1    750 
        \   Scroll Page To Location     ${INDEX} 
        Log    For loop is over 
        Set Selenium Speed  ${DELAY} 

    Scroll Page To Location 
        [Arguments]     ${y_location} 
        Execute JavaScript    window.scrollTo(0,${y_location}) 

and finally the test case-file known as blogtest.robot: 

    *** Settings *** 
    Documentation     A test suite for https://sorhanp.github.io/. 
    Resource          resource.robot 

    *** Test Cases *** 
    Test If Newest Blog Post Is Up 
        Open Browser To Home Page 
        Click The Newest Post 

    iPhone X Test For Blog 
        Resize To iPhone X 
        Capture Page Screenshot 
        Scroll Through Page 

    Test For Blog Images 
        Set Default Browser Size 
        Reload Page 
        Scroll Through Page 
        Blog Should Contain Two Images 

    View Test Completed 
        Go To   https://sorhanp.github.io./assets/images/test_completed.svg 
        [Teardown]    Close Browser

## Final thoughts 

Robot Framework is a highly sophisticated tool for testing. However, in order it for to be most efficient, it is important to design around it from to get go. What I mean by this is that when the robot is accessing web site, it is important to assign IDs for all the elements that are required for testing. For example, if robot needs to check if some picture is visible after an input, that picture should have specific ID so that the robot knows exactly, that it is looking for that picture in particular. There is a term for this kind of approach and it is known as [Test-driven development][tdd]. 

I was also very excited to learn that Robot Framework can be used for [Robotic Process Automation, or RPA][rpa]. It is a way of interacting with a program via a software robot. Robot does exactly what human is doing; thus, it hovers a cursor to a button, clicks it, copies data from form to another etc. Only thing is that it can do it much faster than human, making it perfect tool for automating tasks, such as data entry from one place to another. 

I have been interested in RPA for about a couple of years because I have been part of a workshop designing how RPA would handle automation of certain processes that are error prone and require no actual human input or thought at all. Usually RPA is used in older systems that are no longer viable to develop further to allow native automation to be implemented. I strongly believe that learning more of this will make me a better programmer, since it makes me think ahead of time on how testing can be done effectively.

-sorhanp

[last]: /programming/2019/05/05/DevSecOps-weekly-5.html
[robot_framework]: https://robotframework.org/
[unit_testing]: http://softwaretestingfundamentals.com/unit-testing/
[culture]: https://sorhanp.github.io/programming/2019/04/28/DevSecOps-weekly-4.html
[int_testing]: http://softwaretestingfundamentals.com/integration-testing/
[req_analysis]: https://reqtest.com/requirements-blog/requirements-analysis/
[libraries]: https://robotframework.org/#libraries
[builtin]: http://robotframework.org/robotframework/latest/libraries/BuiltIn.html
[selenium]: http://robotframework.org/SeleniumLibrary/SeleniumLibrary.html#Shortcuts
[tdd]: http://agiledata.org/essays/tdd.html
[rpa]: https://www.uipath.com/rpa/robotic-process-automation